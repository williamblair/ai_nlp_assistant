'''
    Robert Amador, William Blair, Matthew Chorney, Matthew Ochrym, Caleb Williams
    CS ??? / 540 - Artificial Intelligence
    09/20/18
'''

#import subprocess
#import os

# user files
from run_program import run_program
from get_command_TEST import get_command_test
from get_parameters import get_request
import sys
import mail_class
import datetime 
import os
# main entry point
def main():
 
    print("testing file format is tab delimited:")
    print("QUERY    GIVEN_COMMAND\n")
    #   Check for mode
    if len(sys.argv) != 2:
        print("ERROR: Specify Which Mode is desired")
        print("python3 main.py 0 - No Feedback || 1 - Feedback")
        return

    # See if Result Archive is created 
    outLoc = os.getcwd()+"/Results"
    if not (os.path.exists(outLoc)):
            print("FIRST TIME SETUP:    CREATING RESULTS DIRECTORY")
            print("Default Direcory: ",outLoc )
            os.mkdir(outLoc)

    total = 0
    accuracy = 0
    #   Testing mode
    if sys.argv[1] == '0':
        inFile = input("To begin testing, type the testing files name: ")
        # true and exits if the user enters quit
        text_file = open(inFile, "r")
        lines = []
        i = -1
        for line in text_file: 
            lines.append(line.rstrip().split('\t'))
            #   print(lines)
            i+=1
            print("READ-FROM-FILE: "+lines[i][0])
            userInput = lines[i][0]            
            correctClass = lines[i][1]
            guess = get_command_test(userInput, correctClass, sys.argv[1])
            lines[i].append(guess)
            if (guess == correctClass):
                accuracy +=1
        #   Write lines to file
        outFile = outLoc+"/"+inFile[:-4]+"_NF_Result_"+datetime.datetime.today().strftime('%Y-%m-%d_%H-%M-%S')+".txt"
        with open(outFile, 'w') as f:
            for item in lines:
                f.write("%s\t" % item[0])
                f.write("%s\t" % item[1])
                f.write("%s\n" % item[2])
        print(str(accuracy)+"/"+str(i+1))
        f = open(outFile, "a")
        f.write("Accuracy: " + str(accuracy)+"/"+str(i+1) )


    elif sys.argv[1] == '1':
        #   TODO: Figure out how operate auto-feedback
        inFile = input("To begin testing, type the testing files name: ")
        # true and exits if the user enters quit
        text_file = open(inFile, "r")
        lines = []
        i = -1
        for line in text_file: 
            lines.append(line.rstrip().split('\t'))
            #   print(lines)
            i+=1
            print("READ-FROM-FILE: "+lines[i][0])
            userInput = lines[i][0]
            correctClass = lines[i][1]
            guess = get_command_test(userInput, correctClass, sys.argv[1])
            lines[i].append(guess)
            if (guess == correctClass):
                accuracy +=1

        #   Write lines to file
        outFile = outLoc+"/"+inFile[:-4]+"_F_Result_"+datetime.datetime.today().strftime('%Y-%m-%d_%H-%M-%S')+".txt"
        with open(outFile, 'w') as f:
            for item in lines:
                f.write("%s\t" % item[0])
                f.write("%s\t" % item[1])
                f.write("%s\n" % item[2])
        print(str(accuracy)+"/"+str(i+1))
        f = open(outFile, "a")
        f.write("Accuracy: " + str(accuracy)+"/"+str(i+1) )



if __name__ == "__main__":
    main()

