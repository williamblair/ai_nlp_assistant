'''
    Robert Amador, William Blair, Matthew Chorney, Matthew Ochrym, Caleb Williams
    CS ??? / 540 - Artificial Intelligence
    09/20/18
'''

#import subprocess
#import os

# user files
from run_program import run_program
from get_command import get_command
from get_parameters import get_request
import mail_class
# main entry point
def main():


    # dummy greeting
    print('Hello! What would you like to do?')

    # true and exits if the user enters quit
    quit = False

    # loop until the user says so
    while not quit:
        
        # get user input
        userInput = input(">")

        # see if the user wants to exit
        if userInput.split(' ')[0] == 'quit':
            quit = True
            continue
        # get the command from the input
        #command = get_command(userInput)

        # get the parameters from the input
        # this needs some modification
        get_request(userInput)
        
        #print('main command: ' + command)
        #print('main parameters: ' + params)


if __name__ == "__main__":
    main()

