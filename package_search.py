import subprocess


# parse package search lines.

def parseApt(line):

	line = line.split("/",1)[0].split()[0]

	return line


def get_package(package):

	
	cmd = 'apt list *' + package +'*'
	
	process = subprocess.Popen(cmd.split(), stdout = subprocess.PIPE)
	
	try:
		output,err = process.communicate(timeout=20)
	except TimeoutExpired:
		process.kill()
		output,err = process.communicate()
		
	
	# output is a binary string so have to decode to convert, also dont want the first line cuz it's "Listing..."
	real_output = output.decode().splitlines()[1:]

	cmd = 'pip search *' + package + '* | egrep \"^[^ ]*' + package + '[^(]*\"'
	process = subprocess.Popen(cmd, stdout = subprocess.PIPE, shell = True)
	output,err = process.communicate(timeout=20)

	packages = []
	package_manager = []
	
	real_output += list('p') + output.decode().splitlines()[:]
	
	cmd = 'pip3 search *' + package + '* | egrep \"^[^ ]*' + package + '[^(]*\"'
	process = subprocess.Popen(cmd, stdout = subprocess.PIPE, shell = True)
	output,err = process.communicate(timeout=20)

	real_output += list('3') + output.decode().splitlines()[:]

	# if no packages were found then quit
	if(not real_output):
		print("No packages were found")
		return 0,0

	
	print("Apt Available Packages:")

	package_cnt = 0
	apt = 0
	# keep track of which package manager we need to use 0 = apt, 1 = pip, 2 = pip3
	pack_man = 0	

	for line in real_output:
	
		if(line == 'p'):
			print("pip:")
			pack_man = 1
			continue
		elif(line == '3'):
			print("pip3:")
			pack_man = 2
			continue
		elif('INSTALLED' in line):
			continue

		# just cut off unnecessary stuff, only want the package name which is the first item
		packages.append(parseApt(line))
		package_manager.append(pack_man)
	
		print(str(package_cnt)+". "+ str(packages[package_cnt]))
		
		package_cnt += 1
	
	
	print("")
	
	try:
		answer = int(input("What package do you want to install? \nEnter the number of the package to install or any other number for no package."))
	
	except: 
		print("Must enter an integer. Exiting.")
		return 0,0
	
	if(answer >= 0 and answer < package_cnt):
	
		confirm = input("Are you sure you want to install "+str(packages[answer]+"?(y/n)"))

		if(confirm == 'y'):
			return packages[answer],package_manager[answer]
		else:
			return 0,0
	else:
		print("No package chosen")
		return 0,0
	
