# Email function adapted from https://github.com/vprusso/youtube_tutorials/tree/master/utility_scripts/send_email

import smtplib
import config
import sys
import os
import shutil
import datetime 

#   Ask for Email Reciever
def getTo():
    accept = 'N'
    while(accept != 'y'):
        
        to = input("Please enter recipients email: ")
        while '@' not in to:
            print("@ not detected, please provide a valid email")
            to = input("Please enter revcievers email: ")
        accept = input("You entered: "+to+ ", is this correct? [y/n] ")
    return to

#   Ask for Subject Header
def getSubject():
    accept = 'N'
    while(accept != 'y'):
        subject = input("Please enter subject header: ")
        accept = input("You entered: "+subject+ ", is this correct? [y/n] ")
    return subject

def getBody(mailLoc):
    email_body = input("Please enter the email body file name : ")
    bodyArc  = mailLoc+"/"+datetime.datetime.today().strftime('%Y-%m-%d_%H-%M-%S')+"_"+email_body 
    os.system('nano '+bodyArc+".txt")
    return bodyArc


def send_email(subject, msg, to):
    try:
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo()
        server.starttls()
        server.login(config.EMAIL_ADDRESS, config.PASSWORD)
        message = 'Subject: {}\n\n{}'.format(subject, msg)
        server.sendmail(config.EMAIL_ADDRESS, to, message)
        server.quit()
        print("Success: Email sent!")
    except:
        print("Email failed to send.")

var = 'body.txt'
mailLoc = os.getcwd()+"/Mail"

if not (os.path.exists(mailLoc)):
    print("FIRST TIME SETUP:    CREATING MAIL DIRECTORY")
    print("Default Direcory: ",mailLoc )
    os.mkdir(mailLoc)

#   Ask for initial email
to = getTo()
subject = getSubject()
msg = getBody(mailLoc)

#   Confirm entire email
accept = 'N'
while(accept != 'y'):


    print("\nYour Email is: \n")
    print("TO: ",to)
    print("SUBJECT: ",subject)
    fin = open(msg+".txt", 'r') 
    print(fin.read())

    accept = input("Is this message acceptable [y/n] ")
    if (accept == 'n'):
        print("enter [1] to fix Recipient")
        print("enter [2] to fix Subject")
        print("enter [3] to fix the Body")
        fix = input("enter your Choice: ")
        if fix == '1':
            to = getTo()
        elif fix == '2':
            subject = getSubject()
        elif fix == '3':
             os.system('nano '+msg+".txt")

fin = open(msg+".txt", 'r') 
BODY = fin.read()
send_email(subject, BODY, to)

