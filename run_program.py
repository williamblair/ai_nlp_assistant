'''
    William Blair
    09/20/18

    run_program.py - runs a command on the system with arguments,
                     and handles the case that the program does
                     not exist
'''


import subprocess
import os
import webbrowser

# list of executed process objects (not necessarily running)
process_list = []

# takes a list with the first entry being the program and
# the rest of entries are args for that program
# ex: ['progname', 'arg1', 'arg2', 'arg3']
#
# if isWebSearch = True, then either progAndArgsList
# should be a list of size 1 containg the url to go
# to as a string, 
#     e.g. ['https://www.youtube.com']
# or should be a list of strings
# to enter into a google search
#     e.g. [why', 'is', 'the', 'sky', 'blue']
def run_program(progAndArgsList, isWebSearch=False):
    
    try:

        # if we are searching the web
        if isWebSearch:

            # determine if we're going to just a URL or searching
            if len(progAndArgsList) == 1:

                # if just a url then open it
                webbrowser.open(progAndArgsList[0]);

            # otherwise construct a search query string
            else:

                # base string
                urlStr = 'https://www.google.com/search?q='

                # append each argument to the string,
                # each term is separated by a plus sign
                for term in progAndArgsList:
                    if(term == ' '):
                       urlStr += '+'
                    else:
                       urlStr += term

                # finally open the search term
                webbrowser.open(urlStr)

            # we don't want to run the process code below
            return

        # https://docs.python.org/2/library/subprocess.html#subprocess.Popen
        prog = subprocess.Popen(progAndArgsList)

        # add the process to the list (assuming there was no error)
        process_list.append(prog) 

    except OSError:

        print('Hmm, we couldn\'t find', progAndArgsList[0])



