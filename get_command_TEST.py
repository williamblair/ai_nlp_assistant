

from textblob.classifiers import *

def get_command_test(sentence, correctClass, mode):


	with open('train.csv', 'r') as fp:
		cl = NaiveBayesClassifier(fp, format="csv")


	command = cl.classify(sentence)

	
	print("command is " + command)

	#	If testing with Reinforcement
	if mode == '1':
		if command == correctClass:
			print("Correct Assignment	-	No further action")
			answer = "y"
		else:
			print("Inccorrect Assignment 	-	Correcting")
			answer = "n"
		
		#answer = input("Is this correct? (y/n)")
		#	Set to throwaway testing csv for now
		if(answer == "n"):
			f = open("train_TEST.csv", "a")
			sent = correctClass
			#sent = input("What should have been the command?")

			f.write(sentence + "," + sent + "\n")
			f.close()
		
	return command
