

# This is just sort of placeholder stuff for now
# 
from get_command import get_command
from package_search import get_package
from installTest import install
from stopwords import stopWords
from textblob import TextBlob
from run_program import *
from openTest import *
import mail_class

def get_request(sentence):
	EMAIL = mail_class.eMail()
	command = get_command(sentence)


	# if we are going to google, we want the whole sentence in tact
	if(command != 'google'):

		# remove stop words
		se = sentence.split(" ")
		sentence = [i for i in se if i not in stopWords()]
		sentence = ' '.join(sentence)

	nouns = ['NN','NNS','NNP','NNPS']

	b = TextBlob(sentence)

	tags = b.tags
	w = [t for t in tags if t[1] in nouns]

	if(command == 'open'):
		
		if(w):
			program = w[-1][0]

			if(program):
				open(program)

		

	elif(command == 'install'):
		
		if(w):
	
			package,package_manager = get_package(w[-1][0])

			# probably want something like
			if(package):
				install(package,package_manager)
	
		

	elif(command == 'google'):
		run_program(sentence,True)

	elif(command == 'email'):
		EMAIL.callMail()
