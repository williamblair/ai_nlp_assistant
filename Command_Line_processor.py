# -*- coding: utf-8 -*-
"""
Created on Mon Sep 24 05:05:24 2018

@author: oakie
"""
from textblob import TextBlob

#def Learn_Action(action):
#   with open("Actions.txt", "a") as file:
#       print("What is this? Example: program,file,search engine, ...")
#       print("Seperate with only commas, no spaces")
#       file.write('\n' + action + "," + input('')) 

#def Get_Action_List(action):
#    Line = [line.rstrip('\n') for line in open('Actions.txt')]
#    Action_And_Tags = Line.split(',')
#    return action_And_Tags

#def Search_for_Action(Command_list):
#    print("do nothing")

def Get_Run_Command_List():
    return [line.rstrip('\n') for line in open('Run_Commands.txt')]

def Set_New_Command(New_Command):
    with open("Run_Commands.txt", "a") as file:
        file.write('\n' + New_Command)

def Command_Lookup(Find_Command, Unknown_Command):
    Run_Command_list = Get_Run_Command_List()
    Line = 0
    while Line < len(Run_Command_list):
        if Find_Command == Run_Command_list[Line]:
            Set_New_Command(Unknown_Command)
            print("Database Updated")
            Line = len(Run_Command_list)
        elif Line+1 == len(Run_Command_list):
            Command_Lookup(input(Find_Command + ' Unknown, Please list another name for this command: '), Find_Command)
            Command_Lookup(Find_Command, Unknown_Command)
        Line += 1       

def Reader(commands):
    Commands = TextBlob(commands)
    Command_List = Commands.words
    #Search_For_Action(Command_list)
    Run_Command_list = Get_Run_Command_List()
    Line = 0
    while Line < len(Run_Command_list):
        if Command_List[0].singularize() == Run_Command_list[Line]:
            Run(Command_List[1])
            Line = len(Run_Command_list)
        elif Line+1 == len(Run_Command_list):
            Command_Lookup(input(Command_List[0].singularize() + ' Unknown, Please list another name for this command: '), Command_List[0].singularize())
            Reader(commands)
        Line += 1 
    
        
def Run(command):
    print(command)
        
Reader(input('Enter Command: '))